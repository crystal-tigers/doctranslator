package com.crystal.tigers.doctranslator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoctranslatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(DoctranslatorApplication.class, args);
    }

}
