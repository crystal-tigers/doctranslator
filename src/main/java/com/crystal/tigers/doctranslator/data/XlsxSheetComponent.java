package com.crystal.tigers.doctranslator.data;

import lombok.Getter;
import lombok.Setter;

public class XlsxSheetComponent {

    @Getter
    @Setter
    protected XlsxCellComponent[] cellComponents;
}
