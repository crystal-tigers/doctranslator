package com.crystal.tigers.doctranslator.data;

import lombok.Getter;
import lombok.Setter;

public abstract class TextComponent {

    @Getter
    @Setter
    protected String text;

}
