package com.crystal.tigers.doctranslator.data;

import lombok.Getter;
import lombok.Setter;

public class XlsxCellComponent extends TextComponent {

    @Getter
    @Setter
    protected int row;

    @Getter
    @Setter
    protected int column;
}
