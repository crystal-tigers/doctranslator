package com.crystal.tigers.doctranslator.services.translators;

import com.crystal.tigers.doctranslator.entities.ApiHistory;
import com.crystal.tigers.doctranslator.entities.DictEntry;
import com.crystal.tigers.doctranslator.services.dict.ApiHistoryRepository;
import com.crystal.tigers.doctranslator.services.dict.DictEntryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Service("simpleTranslator")
public class SimpleTranslator implements Translator {

    private TranslatorEngine translatorEngine = new YandexTranslator();

    @Autowired
    private DictEntryRepository dictEntryRepository;

    @Autowired
    private ApiHistoryRepository apiHistoryRepository;

    @Override
    public String translate(String text, Locale src, Locale dest) {
        if (dictEntryRepository.findFirstByTextEquals(text) != null) {
            for (DictEntry dictEntry : dictEntryRepository.findFirstByTextEquals(text)) {
                return dictEntry.getTrans();
            }
        }
        String trans = translatorEngine.translate(text, src, dest);
        if (trans != null) {
            dictEntryRepository.save(DictEntry.create(text, trans, src, dest));

            //TODO move to translatorEngine
            apiHistoryRepository.save(ApiHistory.create(text, "yandex", "translate"));
        } else {
            trans = "--";
        }
        return trans;
    }
}
