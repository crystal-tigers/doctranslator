package com.crystal.tigers.doctranslator.services.translators;

import com.crystal.tigers.doctranslator.data.TextComponent;
import com.crystal.tigers.doctranslator.services.parsers.ExcelParser;
import lombok.NonNull;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

@Service
public class SimpleFileHandler implements FileHandler {


    private ExcelParser excelParser = new ExcelParser();

    @Autowired
    private Translator simpleTranslator;

    @Override
    public File translate(MultipartFile multipartFile) {
        return null;
    }

    public File saveFile(MultipartFile multipartFile) {
        try (InputStream is = multipartFile.getInputStream()) {
            File destFile = new File(FileUtils.getTempDirectoryPath()
                    + File.separator + System.nanoTime() + "_" +  multipartFile.getName());
            FileUtils.copyInputStreamToFile(multipartFile.getInputStream(), destFile);
            return destFile;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public File convertAndTranslate(@NonNull MultipartFile file) {

        File savedFile = saveFile(file);
        if (savedFile == null) {
            return null;
        }
        return convertAndTranslate(savedFile);
    }

    public File convertAndTranslate(@NonNull File file) {
        File result = new File(file.getName()+".t.xlsx");
        try {
            XSSFWorkbook workbook = new XSSFWorkbook(file);
            translate(workbook);
            workbook.write(new FileOutputStream(result));
        } catch (InvalidFormatException | IOException e) {
            e.printStackTrace();
            return null;
        }
        return result;
    }

    public void translate(XSSFWorkbook workbook) {

        for (int i = 0; i < workbook.getNumberOfSheets() && i <3; i++) {
            Sheet sheet = workbook.getSheetAt(i);
            translate(sheet);
        }
    }

    public void translate(Sheet sheet) {
        for (Row row : sheet) {
            translate(row);
        }
    }

    public void translate(Row row) {
        for (Cell cell : row) {
            translate(cell);
        }
    }

    //TODO: translate formula?
    public void translate(Cell cell) {
        if (cell.getCellType() == CellType.STRING) {
            String translatedText = simpleTranslator.translate(cell.getStringCellValue(), Locale.JAPANESE, Locale.ENGLISH);
            cell.setCellValue(translatedText);

        }
    }

}
