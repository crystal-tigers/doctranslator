package com.crystal.tigers.doctranslator.services.dict;

import com.crystal.tigers.doctranslator.entities.ApiHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApiHistoryRepository extends JpaRepository<ApiHistory, Long> {

}
