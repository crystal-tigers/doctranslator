package com.crystal.tigers.doctranslator.services.translators;

import java.util.Locale;

public interface Translator {

    //TODO define functions for translate
    String translate(String text, Locale src, Locale dest);
}
