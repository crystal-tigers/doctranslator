package com.crystal.tigers.doctranslator.services.translators;

import org.springframework.web.multipart.*;

import java.io.File;

public interface FileHandler {

    File translate(MultipartFile multipartFile);
}
