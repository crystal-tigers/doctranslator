package com.crystal.tigers.doctranslator.services.translators;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service("yandexTranslator")
public class YandexTranslator implements TranslatorEngine {

    private static final String API_KEY = "trnsl.1.1.20190225T104555Z.f4ffafd0572665f0.f746eea1c722341018482a7b43f3471d9da6ba2a";

    private static final String ENDPOINT = "https://translate.yandex.net/api/v1.5/tr.json/translate";

    public YandexTranslator() {

    }

    @Override
    public String translate(String text, Locale src, Locale dest) {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("key", API_KEY);
        map.add("text", text);
        map.add("lang", src.toString() + "-" + dest.toString());

        HttpEntity<MultiValueMap<String, String>> request =
                new HttpEntity<MultiValueMap<String, String>>(map, headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.postForEntity( ENDPOINT, request , String.class );

        String responseBody = response.getBody();
        try {
            JsonNode rootNode = new ObjectMapper().readTree(responseBody);
            List<String> resTranslations = new ArrayList<>();
            for (JsonNode node : rootNode.findValue("text")) {
                resTranslations.add(node.toString());
            }
            return String.join("\n", resTranslations);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
