package com.crystal.tigers.doctranslator.services.dict;

import com.crystal.tigers.doctranslator.entities.DictEntry;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DictEntryRepository extends JpaRepository<DictEntry, Long> {

    Iterable<DictEntry> findFirstByTextEquals(String text);
}
