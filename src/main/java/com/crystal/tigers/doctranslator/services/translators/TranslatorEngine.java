package com.crystal.tigers.doctranslator.services.translators;

import java.util.Locale;

public interface TranslatorEngine {

    String translate(String text, Locale src, Locale dest);
}
