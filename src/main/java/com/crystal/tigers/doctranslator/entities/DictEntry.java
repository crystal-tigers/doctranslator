package com.crystal.tigers.doctranslator.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Locale;

@Entity
public class DictEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    private Long id;

    @Getter
    @Setter
    @Column(columnDefinition = "text")
    private String text;

    @Getter
    @Setter
    @Column(columnDefinition = "text")
    private String trans;

    @Getter
    @Setter
    private Locale srcLocale;

    @Getter
    @Setter
    private Locale destLocale;

    public static DictEntry create(String text, String trans, Locale srcLocale, Locale destLocale) {
        DictEntry dictEntry = new DictEntry();
        dictEntry.text = text;
        dictEntry.trans = trans;
        dictEntry.srcLocale = srcLocale;
        dictEntry.destLocale = destLocale;
        return dictEntry;
    }
}
