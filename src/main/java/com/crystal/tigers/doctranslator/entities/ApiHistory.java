package com.crystal.tigers.doctranslator.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class ApiHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    private long id;

    @Getter
    @Setter
    private String apiName;

    @Getter
    @Setter
    private String action;

    @Getter
    @Setter
    private long count;

    @Getter
    @Setter
    private Timestamp timestamp;

    public static ApiHistory create(String text, String apiName, String action) {
        ApiHistory apiHistory = new ApiHistory();
        apiHistory.apiName = apiName;
        apiHistory.count = text.length();
        apiHistory.action = action;
        apiHistory.timestamp = new Timestamp(System.currentTimeMillis());
        return apiHistory;
    }
}
