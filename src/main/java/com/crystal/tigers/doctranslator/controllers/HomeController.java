package com.crystal.tigers.doctranslator.controllers;


import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.*;

@Controller
public class HomeController {

    //TODO: home page
    @GetMapping("/")
    public String index() {
        return "index";
    }

    //TODO: upload file page

    //TODO: upload file handle
    @PostMapping("/upload")
    public String upload(@RequestParam("doc") MultipartFile uploadedFile) {
        return "upload";
    }
}
