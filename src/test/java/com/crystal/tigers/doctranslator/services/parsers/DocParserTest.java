package com.crystal.tigers.doctranslator.services.parsers;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.SimpleShape;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFShape;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DocParserTest {

    @Test
    public void contextLoads() {

    }



    @Test
    public void testRun() {
        final String filename = "/Users/sonyynoss/Documents/adrepo.xlsx";
        try {
            FileInputStream excelFile = new FileInputStream(new File(filename));
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            XSSFSheet sheet0 = (XSSFSheet) datatypeSheet;
            XSSFDrawing drawingSheet0 = sheet0.createDrawingPatriarch();
            for (XSSFShape shape : drawingSheet0.getShapes()) {
                if (shape instanceof SimpleShape) {
                    XSSFSimpleShape simpleShape = (XSSFSimpleShape) shape;
                    System.out.println(simpleShape.getText());
                }
            }

            for (Row currentRow : datatypeSheet) {
                for (Cell currentCell : currentRow) {
                    //getCellTypeEnum shown as deprecated for version 3.15
                    //getCellTypeEnum ill be renamed to getCellType starting from version 4.0
                    if (currentCell.getCellType() == CellType.STRING) {
                        System.out.print(currentCell.getStringCellValue() + "--");
                    } else if (currentCell.getCellType() == CellType.NUMERIC) {
                        System.out.print(currentCell.getNumericCellValue() + "--");
                    }

                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

}
