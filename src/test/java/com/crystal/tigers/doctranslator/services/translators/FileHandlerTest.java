package com.crystal.tigers.doctranslator.services.translators;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FileHandlerTest {

    @Autowired
    SimpleFileHandler fileHandler1;

    @Test
    public void loadFileWithContent() throws IOException {

    }

    @Test
    public void testSaveFile() throws IOException {
        MultipartFile file = mock(MultipartFile.class);
        when(file.getName()).thenReturn("testCreateCopyFile_shouldneverexists.xlsx");
        when(file.getInputStream())
                .thenReturn(getClass().getClassLoader().getResourceAsStream("test-files/adrepo.xlsx"));
        File copiedFile = fileHandler1.saveFile(file);
        Assert.assertNull(copiedFile);

    }

    @Test
    public void testTranslate() {
        File result = fileHandler1.
                convertAndTranslate(new File(getClass().getClassLoader().getResource("test-files/2017Q4_j.xlsx").getPath()));
        System.out.println(result.getAbsolutePath());
        Assert.assertTrue(result.exists());
    }
}
